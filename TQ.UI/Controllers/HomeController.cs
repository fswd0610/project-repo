﻿using System.Linq;
using System.Web.Mvc;
using TQ.Data;

namespace TQ.UI.Controllers
{
    public class HomeController : Controller
    {
        private QuestionsEntities db = new QuestionsEntities();

        // GET: Questions
        public ActionResult Index()
        {
            return View(db.Questions.ToList());
        }

        public ActionResult Random()
        {
            return View(db.Questions.ToList());
        }
        public ActionResult List()
        {
            return View(db.CategoryQuestionLinks.ToList());
        }

        [HttpGet]

        public ActionResult About()
        {

            return View();
        }

        [HttpGet]
        public ActionResult Contact()
        {

            return View();
        }
    }
}
