﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TQ.Data;

namespace TQ.UI.Controllers
{
    public class CategoryQuestionLinksController : Controller
    {
        private QuestionsEntities db = new QuestionsEntities();

        // GET: CategoryQuestionLinks
        [Authorize]
        public ActionResult Index()
        {
            var categoryQuestionLinks = db.CategoryQuestionLinks.Include(c => c.Category).Include(c => c.Question);
            return View(categoryQuestionLinks.ToList());
        }

        // GET: CategoryQuestionLinks/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CategoryQuestionLink categoryQuestionLink = db.CategoryQuestionLinks.Find(id);
            if (categoryQuestionLink == null)
            {
                return HttpNotFound();
            }
            return View(categoryQuestionLink);
        }

        // GET: CategoryQuestionLinks/Create
        [Authorize]
        public ActionResult Create()
        {
            ViewBag.CID = new SelectList(db.Categories, "CID", "Name");
            ViewBag.QID = new SelectList(db.Questions, "QID", "Question1");
            return View();
        }

        // POST: CategoryQuestionLinks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Create([Bind(Include = "QID,CID,LinkID")] CategoryQuestionLink categoryQuestionLink)
        {
            if (ModelState.IsValid)
            {
                db.CategoryQuestionLinks.Add(categoryQuestionLink);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CID = new SelectList(db.Categories, "CID", "Name", categoryQuestionLink.CID);
            ViewBag.QID = new SelectList(db.Questions, "QID", "Question1", categoryQuestionLink.QID);
            return View(categoryQuestionLink);
        }

        // GET: CategoryQuestionLinks/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CategoryQuestionLink categoryQuestionLink = db.CategoryQuestionLinks.Find(id);
            if (categoryQuestionLink == null)
            {
                return HttpNotFound();
            }
            ViewBag.CID = new SelectList(db.Categories, "CID", "Name", categoryQuestionLink.CID);
            ViewBag.QID = new SelectList(db.Questions, "QID", "Question1", categoryQuestionLink.QID);
            return View(categoryQuestionLink);
        }

        // POST: CategoryQuestionLinks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult Edit([Bind(Include = "QID,CID,LinkID")] CategoryQuestionLink categoryQuestionLink)
        {
            if (ModelState.IsValid)
            {
                db.Entry(categoryQuestionLink).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CID = new SelectList(db.Categories, "CID", "Name", categoryQuestionLink.CID);
            ViewBag.QID = new SelectList(db.Questions, "QID", "Question1", categoryQuestionLink.QID);
            return View(categoryQuestionLink);
        }

        // GET: CategoryQuestionLinks/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CategoryQuestionLink categoryQuestionLink = db.CategoryQuestionLinks.Find(id);
            if (categoryQuestionLink == null)
            {
                return HttpNotFound();
            }
            return View(categoryQuestionLink);
        }

        // POST: CategoryQuestionLinks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize]
        public ActionResult DeleteConfirmed(int id)
        {
            CategoryQuestionLink categoryQuestionLink = db.CategoryQuestionLinks.Find(id);
            db.CategoryQuestionLinks.Remove(categoryQuestionLink);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
