﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TQ.Data//.Metadata
{
    [MetadataType(typeof(CategoryMetadata))]
    public partial class Category { }
        public class CategoryMetadata
    {

        public int CID { get; set; }
        [Required(ErrorMessage = "Required")]
        [Display(Name = "Category")]
        public string Name { get; set; }
    }
}
