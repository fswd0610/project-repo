﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TQ.Data//.Metadata
{
    [MetadataType(typeof(QuestionMetadata))]
    public partial class Question { }
    public class QuestionMetadata
    {


        public int QID { get; set; }


        [Display(Name = "Question")]
        [Required(ErrorMessage = "Required")]
        public string Question1 { get; set; }


        [Display(Name = "Answer")]
        [Required(ErrorMessage = "Required")]
        public string Answer { get; set; }

    }
}
